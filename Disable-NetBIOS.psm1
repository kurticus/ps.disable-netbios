<#
.SYNOPSIS
	Disables NetBIOS
.DESCRIPTION
	Disables NetBIOS on the system the script is run
.PARAMETER output
    LogPath - The directory path that logs are stored
.EXAMPLE
    Disable-NetBIOS -LogPath "C:\Temp"
.NOTES
	Author:       Kurt Marvin
	
	Changelog:
	   1.0        Initial release
#>

#region ##################### Parameters #####################
Param 
(
	#[Parameter(Mandatory=$true)]
	[string]$LogPath = "C:\Temp"
)
#endregion

#region ##################### Variables #####################

# The Registry Key where the network interfaces is stored
$RegistryKey = 'HKLM:\SYSTEM\CurrentControlSet\Services\netbt\Parameters\interfaces'
# The filename to use for the log file
$LogFileName = "Disable-NetBIOS-log-$((Get-Date).Year)-$((Get-Date).Month)-$((Get-Date).Day).log"

#endregion

#region ##################### Functions #####################
function Write-Log 
{
    Param 
    (
        [string]$output = ""
    )

    # Create log directory if it doesn't exist
    if (-Not(Test-Path $LogPath)) { New-Item -Path $LogPath -ItemType Directory }

    $path = $LogPath + "\" + $LogFileName
    $output = (Get-Date).DateTime + " | " + $output
    Add-Content -Path $path -Value $output
}
#endregion

#region ##################### Main #####################
Write-Log "Starting the script"

Get-ChildItem $RegistryKey | ForEach-Object { 
    Write-Log "Network Interface Found - $RegistryKey\$($_.pschildname)"
    Set-ItemProperty -Path "$RegistryKey\$($_.pschildname)" -name NetBiosOptions -value 2
    Write-Log "Network Interface NetBIOS Disabled - $RegistryKey\$($_.pschildname)"
}

Write-Log "Ending the script"
#endregion